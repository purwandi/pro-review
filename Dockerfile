FROM gliderlabs/alpine
MAINTAINER Purwandi

RUN apk-install ca-certificates nginx
COPY ./ /usr/share/nginx/html

EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]